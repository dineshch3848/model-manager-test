package com.predera.dmml.web.rest.vm;

public class ModelHealthDTO {

    private Object timestamp;

    private int health;
    
    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public String toString() {
        return "ModelHealthDTO [timestamp=" + timestamp + ", health=" + health + "]";
    }
}
