package com.predera.dmml.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.validation.Valid;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.predera.dmml.config.ApplicationProperties;
import com.predera.dmml.security.SecurityUtils;
import com.predera.dmml.service.ModelUploadService;
import com.predera.dmml.service.ModelsDataService;
import com.predera.dmml.service.PredictionsDataService;
import com.predera.dmml.service.dto.GetAllModelsDTO;
import com.predera.dmml.service.dto.PredictionDTO;
import com.predera.dmml.service.dto.PredictionResponseDTO;
import com.predera.dmml.web.rest.errors.BadRequestAlertException;
import com.predera.dmml.web.rest.errors.InternalServerErrorException;
import com.predera.dmml.web.rest.util.HeaderUtil;
import com.predera.dmml.web.rest.vm.BooleanNotificationValueDTO;
import com.predera.dmml.web.rest.vm.CreateModelDTO;
import com.predera.dmml.web.rest.vm.GetAllProjectsDTO;
import com.predera.dmml.web.rest.vm.GetAllProjectsNamesDTO;
import com.predera.dmml.web.rest.vm.ModelHealthDTO;
import com.predera.dmml.web.rest.vm.PredictionTrendDTO;
import com.predera.dmml.web.rest.vm.ReplicationFactorDTO;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Model.
 */
@SuppressWarnings("deprecation")
@RestController
@RequestMapping("/api")
public class ModelResource {

	private final Logger log = LoggerFactory.getLogger(ModelResource.class);

	private static final String ENTITY_NAME = "model";

	private final String modelDeployUrl;
	private final String modelUndeployUrl;
	private final String mmPredictionUrl;
	private final String replicationFactorUrl;
	private final String pymmPredictionUrl;
	private String predictionIndex;
    private String predictionDocType;
	private final ObjectMapper jsonMapper;
	private final RestTemplate restTemplate;
	private final PredictionsDataService predictionDataService;
	private final ModelsDataService modelDataService;

	@Autowired
	private ModelUploadService modelUploadService;

	public ModelResource(PredictionsDataService predictionDataService, ModelUploadService modelUploadService,
                         ApplicationProperties applicationProperties, ModelsDataService modelDataService) {
        this.predictionDataService = predictionDataService;
        this.modelUploadService = modelUploadService;
		this.modelDataService = modelDataService;
		
		this.predictionIndex = applicationProperties.getDataService().getElasticsearch().getPredictions().getIndex();
		this.predictionDocType = applicationProperties.getDataService().getElasticsearch().getPredictions().getDocType();
		this.mmPredictionUrl = applicationProperties.getModelManager().getPredictionUrl();
		this.pymmPredictionUrl = applicationProperties.getPyModelManager().getPredictionUrl();
		this.modelDeployUrl = applicationProperties.getPyModelManager().getModelDeployUrl();
		this.modelUndeployUrl = applicationProperties.getPyModelManager().getModelUndeployUrl();
		this.replicationFactorUrl = applicationProperties.getPyModelManager().getReplicationFactorUrl();

		this.jsonMapper = new ObjectMapper();
		this.restTemplate = new RestTemplate();
	}

	/**
	 * POST /models : Create a new model.
	 *
	 * @param inputModel
	 *            the model to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         model, or with status 400 (Bad Request) if the model has already an
	 *         ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws IOException
	 */
	@PostMapping("/models")
	@Timed
	public ResponseEntity<CreateModelDTO> createModel(@Valid @RequestBody CreateModelDTO inputModel) throws URISyntaxException, IOException {
	    String loginUser = SecurityUtils.getCurrentUserLogin().get();
		CreateModelDTO result = modelDataService.saveModel(inputModel, loginUser);
        return ResponseEntity.created(new URI("/api/models/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

    /*
	/**
	 * PUT /models : Updates an existing model.
	 *
	 * @param updateModel
	 *            the model to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         model, or with status 400 (Bad Request) if the model is not valid, or
	 *         with status 500 (Internal Server Error) if the model couldn't be
	 *         updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	@SuppressWarnings("static-access")
	@PutMapping("/models")
	@Timed
	public ResponseEntity<Model> updateModel(@Valid @RequestBody UpdateModelDto updateModel) throws URISyntaxException {
		log.debug("REST request to update Model : {}");

		Model model = new Model();
		model.setName(updateModel.getName());
		model.setType(updateModel.getType());
		model.setAlgorithm(updateModel.getAlgorithm());
		model.setModelLocation(updateModel.getModelLocation());
		model.setPerformanceMetrics(updateModel.getPerformanceMetrics());
		model.setBuilderConfig(updateModel.getBuilderConfig());
		model.setFeatureSignificance(updateModel.getFeatureSignificance());
		model.setTrainingDataset(updateModel.getTrainingDataset());

		Model result = modelService.save(model);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, model.getId().toString()))
				.body(result);
	}
	*/

	/**
	 * GET /models : get all the models.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of models in body
	 * @throws IOException 
	 */
	@GetMapping("/models")
	@Timed
	public ResponseEntity<List<GetAllModelsDTO>> getAllModels() throws IOException{
	    List<GetAllModelsDTO> result = modelDataService.getAllModels();
		return new ResponseEntity<List<GetAllModelsDTO>>(result, HttpStatus.OK);
	}

	/**
	 * GET :models/count : get number of models for current user
	 *
	 * @return number of models for current user
	 * @throws IOException 
	 */
	@GetMapping("/models/count")
	@Timed
	public Long getCountOfModels() throws IOException {
		String owner = SecurityUtils.getCurrentUserLogin().get();
		log.debug("REST request to get count of models by owner: {}", owner);
		
		return modelDataService.getCount();
	}

	/**
	 * GET /models/:id : get the "id" model.
	 *
	 * @param id
	 *            the id of the model to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the model, or
	 *         with status 404 (Not Found)
	 * @throws IOException 
	 */
	@GetMapping("/models/{id}")
	@Timed
	public ResponseEntity<CreateModelDTO> getModel(@PathVariable String id) throws IOException {
		log.debug("REST request to get Model : {}", id);
		String loginUser = SecurityUtils.getCurrentUserLogin().get();
		CreateModelDTO model = modelDataService.getModelById(id);
		if(model.getOwner().equals(loginUser)) {
		    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(model));
		}
		else {
		    throw new BadRequestAlertException("Invalid permissions", "Model", "invalid");
		}
	}

    /**
     * DELETE /models/:id : delete the "id" model.
     *
     * @param id
     *            the id of the model to delete
     * @return the ResponseEntity with status 200 (OK)
     * @throws IOException
     */
    @DeleteMapping("/models/{id}")
    @Timed
    public ResponseEntity<Void> deleteModel(@PathVariable String id) throws IOException {
        log.debug("REST request to delete Model : {}", id);
        CreateModelDTO model = modelDataService.getModelById(id);
        if (model == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (model.getStatus().equals("Deployed") || model.getStatus().equals("deployed")) {
            throw new BadRequestAlertException("You cannot delete a Deployed Model", "Model", "invalid");
        }
        String path = model.getModelLocation();
        try {
            if (path != null) {
                Files.deleteIfExists(Paths.get(path));
                modelDataService.delete(id);
                return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                        .build();
            } else {
                modelDataService.delete(id);
                return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                        .build();
            }
        } catch (NoSuchFileException e) {
            throw new BadRequestAlertException("No such file/directory exists", "Model", "Not Exists");
        } catch (IOException e) {
            throw new BadRequestAlertException("Invalid permissions", "Model", "invalid");
        }
    }

	/**
	 * POST /models/:id/deploy : Deploy a model.
	 *
	 * @param id
	 *            the id of the model to deploy
	 * @return the ResponseEntity with status 200 (Ok) or with status 400 (Bad
	 *         Request) if the request is invalid
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
    @PostMapping("/models/{id}/deploy")
    @Timed
    public ResponseEntity<CreateModelDTO> deployModel(@PathVariable String id, 
                                             @RequestBody ReplicationFactorDTO replicationFactor) 
                                             throws Exception {
        log.debug("REST request to get Model : {}", id);
        log.debug("Replication Factor : {}", replicationFactor);
        
        log.debug("Replication Factor : {}", replicationFactor.getReplicationFactor());

		String currentUser = SecurityUtils.getCurrentUserLogin().get();
		CreateModelDTO model = modelDataService.getModelById(id);
		if (model == null) {
			log.debug("Model not found or user {} does not have access to model {}", currentUser, id);
			throw new BadRequestAlertException("Model not found or access denied", "Model", "invalid");
		}

		Map builderConfig = jsonMapper.readValue(jsonMapper.writeValueAsString(model.getBuilderConfig()), Map.class);
		log.debug("Builder Config: {}", builderConfig);
		log.debug("Model Builder Library: {}", builderConfig.get("library"));

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(modelDeployUrl)
            .queryParam("model_name", model.getName())
            .queryParam("model_path", model.getModelLocation())
            .queryParam("model_type", model.getType())
            .queryParam("model_library", builderConfig.get("library"))
            .queryParam("replication_factor", replicationFactor.getReplicationFactor());
		// TODO: Move this to a deployment service
		ResponseEntity<String> deployResponse = restTemplate.postForEntity(builder.build().toUri(), null, String.class);
		log.debug("py-model-manager response: {}", deployResponse);

		if (!deployResponse.getStatusCode().is2xxSuccessful()) {
			log.debug("Model deployment failed, returning py-model-manager response to client");
			throw new InternalServerErrorException("Model deployment failed!");
		}

		log.debug("Model deployed successfully using py-model-manager, updating status in db");
		String replicas = replicationFactor.getReplicationFactor();
		String predictionUrl = this.mmPredictionUrl.replaceFirst("\\{id\\}", model.getId());
		CreateModelDTO result = modelDataService.updateModelAfterDeployment(id, replicas, predictionUrl); 
		        
		ResponseEntity<CreateModelDTO> response = ResponseEntity.ok().body(result);
		log.debug("Success response: {}", response);
		return response;
	}

	/**
     * POST /models/:id/scale : Set replication factor a model after deployment.
     *
     * @param id
     *            the id of the model deployed
     * @return the ResponseEntity with status 200 (Ok) or with status 400 (Bad
     *         Request) if the request is invalid
     * @throws URISyntaxException
     *             if the Location URI syntax is incorrect
     */
    @PostMapping("/models/{id}/scale")
    @Timed
    public ResponseEntity<CreateModelDTO> replicationFactor(@PathVariable String id, @RequestBody ReplicationFactorDTO replicationFactor) throws Exception {
        log.debug("REST request to get Model : {}", id);
        String currentUser = SecurityUtils.getCurrentUserLogin().get();
        CreateModelDTO model = modelDataService.getModelById(id);;
        if (model == null) {
            log.debug("Model not found or user {} does not have access to model {}", currentUser, id);
            throw new BadRequestAlertException("Model not found or access denied", "Model", "invalid");
        }
        
        if (model.getStatus().equals("Experiment")) {
            log.debug("Model is not deployed", currentUser, id, model.getStatus());
            throw new BadRequestAlertException("Cannot set replication factor for undeployed models", "Model", "invalid");
        }
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(jsonMapper.writeValueAsString(replicationFactor), headers);
        ResponseEntity<String> replicationFactorResponse = restTemplate.postForEntity(replicationFactorUrl, entity, String.class,
                model.getName());
        log.debug("py-model-manager response: {}", replicationFactorResponse);

        if (!replicationFactorResponse.getStatusCode().is2xxSuccessful()) {
            log.debug("Set replication factor for deployed model failed, returning py-model-manager response to client");
            throw new InternalServerErrorException("Set replication factor for deployed model failed!");
        }

        log.debug("Replication factor set to deployed model successfully using py-model-manager");
        
        log.debug("replication Factor {}", replicationFactor.getReplicationFactor());
        
        String replicas = replicationFactor.getReplicationFactor();
        CreateModelDTO result = modelDataService.updateAfterScaling(id, replicas);
        
        ResponseEntity<CreateModelDTO> response = ResponseEntity.ok().body(result);
        log.debug("Success response: {}", response);
        return response;
    }
    
    @GetMapping("/predictions/{id}")
    @Timed
    public ResponseEntity<PredictionDTO> getPredictionById(@PathVariable("id") String id) throws IOException {
        log.debug("Rest request to get prediction by using model id: {}", id);
        
        PredictionDTO result = predictionDataService.getPredictionById(id);
        ResponseEntity<PredictionDTO> response = ResponseEntity.ok().body(result);
        log.debug("Success response: {}", response);
        return response;
    }
    
    @PostMapping("/models/{id}/notification/update")
    @Timed
    public ResponseEntity<CreateModelDTO> updateNotificationValue(@PathVariable("id") String id, @RequestBody BooleanNotificationValueDTO notificationValue) throws IOException {
        log.debug("Rest request to update notification value to the model: {}", id);
        
        String currentUser = SecurityUtils.getCurrentUserLogin().get();
        CreateModelDTO model = modelDataService.getModelById(id);
        if (model == null) {
            log.debug("Model not found or user {} does have access to model {}", currentUser, id);
            throw new BadRequestAlertException("Model not found or access denied", ENTITY_NAME,
                "Model not found or access denied");
        }
        
        CreateModelDTO result = modelDataService.updateNotificationValue(id, notificationValue.isNotificationValue());
        ResponseEntity<CreateModelDTO> response = ResponseEntity.ok().body(result);
        log.debug("Success response: {}", response);
        return response;
    }

	/**
	 * POST /models/{id}/undeploy : Undeploy a model.
	 *
	 * @param id
	 *            the id of the model to undeploy
	 * @return the ResponseEntity with status 200 (Ok) or with status 400 (Bad
	 *         Request) if the request is invalid
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/models/{id}/undeploy")
	@Timed
	public ResponseEntity<CreateModelDTO> undeployModel(@PathVariable String id) throws Exception {
		log.debug("REST request to undeploy Model : {}", id);

		String currentUser = SecurityUtils.getCurrentUserLogin().get();
		CreateModelDTO model = modelDataService.getModelById(id);
		if (model == null) {
			log.debug("Model not found or user {} does not have access to model {}", currentUser, id);
			throw new BadRequestAlertException("Model not found or access denied", "Model", "invalid");
		}

		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(modelUndeployUrl).queryParam("model_name",
				model.getName());
		// TODO: Move this to a deployment service
		ResponseEntity<String> undeployResponse = restTemplate.postForEntity(builder.build().toUri(), null,
				String.class);
		log.debug("py-model-manager response: {}", undeployResponse);

		if (!undeployResponse.getStatusCode().is2xxSuccessful()) {
			log.debug("Model undeployment failed, returning py-model-manager response to client");
			throw new InternalServerErrorException("Error undeploying the model!");
		}

		log.debug("Model undeployed successfully using py-model-manager, updating status in db");
		
		CreateModelDTO result = modelDataService.updateModelAfterUnDeployment(id);

		ResponseEntity<CreateModelDTO> response = ResponseEntity.ok().body(result);
		log.debug("Success response: {}", response);
		return response;
	}

	/**
	 * POST /models/:id/predict : predict using the model
	 *
	 * @param id the id of the model to use for prediction
     * @param uid (optional) unique identifier e.g. user id
	 * @param input request data
	 * @return the ResponseEntity with status 200 (OK) and prediction output in body
	 *         or with status 400 (Bad Request) if the request is not valid
	 * @throws IOException
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 * @throws JSONException
	 */
    @PostMapping("/models/{id}/predict")
    @Timed
    public ResponseEntity<PredictionDTO> predict(@PathVariable String id, @RequestParam(value = "uid", required=false) String uid,
                                              @RequestBody String input) throws IOException, InterruptedException, ExecutionException {
        log.debug("REST request to predict using Model: {}", id);
        log.debug("uid: {}", uid);
        log.debug("Prediction Input : {}", input);

        String currentUser = SecurityUtils.getCurrentUserLogin().get();
        CreateModelDTO model = modelDataService.getModelById(id);
        if (model == null) {
            log.debug("Model not found or user {} does have access to model {}", currentUser, id);
            throw new BadRequestAlertException("Model not found or access denied", ENTITY_NAME,
                "Model not found or access denied");
        }

        Map<String, Object> jsonInput;
        try {
            jsonInput = jsonMapper.readValue(input, Map.class);
            log.debug("JSON Input: {}", jsonInput);
        } catch (IOException e) {
            log.debug("Invalid request: {}", e.getMessage());
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(input, headers);
        // TODO: Move this to prediction service
        ResponseEntity<String> pymmResponse = restTemplate.postForEntity(pymmPredictionUrl, entity, String.class,
            model.getName());
        PredictionResponseDTO predictionResponseDTO = jsonMapper.readValue(pymmResponse.getBody(), PredictionResponseDTO.class);
        log.debug("Prediction Response DTO : {}", predictionResponseDTO);

        CompletableFuture<PredictionDTO> prediction = predictionDataService.asyncSavePrediction(model, jsonInput, predictionResponseDTO, uid);
        PredictionDTO predictionDTO = new PredictionDTO();
        predictionDTO.setId(prediction.get().getId());
        predictionDTO.setIndex(predictionIndex);
        predictionDTO.setType(predictionDocType);
        predictionDTO.setModelId(model.getId());
        predictionDTO.setModelName(model.getName());
        predictionDTO.setProject(model.getProject());
        predictionDTO.setVersion(model.getVersion());
        predictionDTO.setUid(uid);
        predictionDTO.setPredictionInput(jsonInput);
        predictionDTO.setPredictionDate(Date.from(Instant.now()));
        predictionDTO.setPredictionOutput(predictionResponseDTO.getOutput().getResult());
        predictionDTO.setPredictionReasons(predictionResponseDTO.getOutput().getReasons());

        ResponseEntity<PredictionDTO> response = ResponseEntity.ok(predictionDTO);
        log.debug("REST response: {}", response.getBody());
        return response;
    }

	/**
	 * POST /models/:id/predictions : Predictions of a model.
	 *
	 * @param id
	 *            the id of the model to get predictions.
	 * @return the ResponseEntity with status 200 (Ok) or with status 400 (Bad
	 *         Request) if the request is invalid
	 */
	@GetMapping("/models/{id}/predictions")
	public ResponseEntity<List<PredictionDTO>> getPredictionsById(@PathVariable("id") String id) throws IOException{
        List<PredictionDTO> result = predictionDataService.getPredictionsById(id);
        return new ResponseEntity<List<PredictionDTO>>(result, HttpStatus.OK);
    }
	
	/**
     * POST /models/:id/feedbacks : feedbacks of a model.
     *
     * @param id
     *            the id of the model to get feedbacks.
     * @return the ResponseEntity with status 200 (Ok) or with status 400 (Bad
     *         Request) if the request is invalid
     */
    @GetMapping("/models/{id}/feedbacks")
    public ResponseEntity<List<PredictionDTO>> getFeedbacksById(@PathVariable("id") String id) throws IOException{
        List<PredictionDTO> result = predictionDataService.getFeedbacksById(id);
        return new ResponseEntity<List<PredictionDTO>>(result, HttpStatus.OK);
    }

    /**
     * POST /models/:id/upload : Upload a model.
     *
     * @param id the id of the model which is being uploaded
     * @param file model file
     * @return the file path where it is uploaded with status 200 (Ok) or with
     *         status 400 (Bad Request) if the request is invalid
     * @throws IOException 
     */
    @PostMapping("/models/{id}/upload")
    @Timed
    public ResponseEntity<CreateModelDTO> uploadFile(@PathVariable("id") String id,
                                            @RequestParam("file") MultipartFile file) throws IOException {
        log.debug("REST request to upload Model : {}", id);
        if (file == null) {
            throw new BadRequestAlertException("Model file is required", ENTITY_NAME, "invalid");
        }
        log.debug("Model file : {}", file);

        CreateModelDTO model = modelDataService.getModelById(id);
        if (model == null) {
            throw new BadRequestAlertException("Model not found or User does not have access", ENTITY_NAME, "notfound");
        }
        if (model.getModelLocation() != null) {
            log.debug("Model location: {}", model.getModelLocation());
            throw new BadRequestAlertException("Model file already exists", ENTITY_NAME, "fileexists");
        }

        Path modelPath = modelUploadService.store(file, model.getName(), model.getVersion());
        String modelLocation = modelPath.toUri().getPath();
        log.debug("Model saved as : {}", modelLocation);

        model.setModelLocation(modelLocation);
        CreateModelDTO result = modelDataService.updateModelLocation(id, modelLocation);

        ResponseEntity<CreateModelDTO> response = ResponseEntity.ok().body(result);
        log.debug("Success response: {}", response);
        return response;
    }
    
    /**
     * GET /models/:id/health/trend : gets trend of model health.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of models health in body
     * @throws IOException 
     */
    @GetMapping("/models/{id}/health/trend")
    @Timed
    public ResponseEntity<List<ModelHealthDTO>> getModelHealth(@PathVariable("id") String id) throws IOException{
        log.debug("Getting  health trend");
        List<ModelHealthDTO> result = predictionDataService.getModelHealth(id);
        log.debug("Result :{}", result);
        return new ResponseEntity<List<ModelHealthDTO>>(result, HttpStatus.OK);
        
    }
    
    /**
     * GET /models/:id/predictions/trend : gets trend of model predictions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of models predictions in body
     * @throws IOException 
     */
    @GetMapping("/models/{id}/predictions/trend")
    @Timed
    public ResponseEntity<List<PredictionTrendDTO>> getModelTrend(@PathVariable("id") String id) throws IOException{
        log.debug("Getting  prediction trend");
        List<PredictionTrendDTO> result = predictionDataService.getPredictionTrend(id);
        log.debug("Result :{}", result);
        return new ResponseEntity<List<PredictionTrendDTO>>(result, HttpStatus.OK);
    }
    
    /**
     * GET /models/:projectName : get all the models according to the project name.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of models in body 
     * @throws IOException 
     */
    @GetMapping("/projects/{projectName}")
    @Timed
    public ResponseEntity<List<GetAllModelsDTO>> getAllModelsByProject(@PathVariable("projectName") String project) throws IOException {
        log.debug("Searching models of project : {}", project);
        List<GetAllModelsDTO> result = modelDataService.getAllModelsByProject(project);
        return new ResponseEntity<List<GetAllModelsDTO>>(result, HttpStatus.OK);
    }
    
    /**
     * GET /projects : get all projects.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of projects
     * @throws IOException 
     */
    @GetMapping("/projects")
    @Timed
    public ResponseEntity<List<GetAllProjectsDTO>> getAllProjects() throws IOException{
        log.debug("Searching All projects");
        List<GetAllProjectsDTO> result = modelDataService.getAllProject();
        return new ResponseEntity<List<GetAllProjectsDTO>>(result, HttpStatus.OK);
    }
    
    /**
     * GET /projects/names : get unique projects names.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of projects
     * @throws IOException 
     */
    @GetMapping("/projects/names")
    @Timed
    public ResponseEntity<List<GetAllProjectsNamesDTO>> getAllProjectsNames() throws IOException{
        log.debug("Searching All projects");
        List<GetAllProjectsNamesDTO> result = modelDataService.getAllProjectsNames();
        return new ResponseEntity<List<GetAllProjectsNamesDTO>>(result, HttpStatus.OK);
    }
    
    
    
//	/**
//	 * POST /models/:modelId/predictions/:predictionId/feedback : feedback of a model.
//	 *
//	 * @param modelId
//	 *
//	 * @param predictionId
//	 *
//	 * @return the ResponseEntity with status 200 (Ok) or with status 400 (Bad
//	 *         Request) if the request is invalid
//	 */
//	@SuppressWarnings("unchecked")
//	@PostMapping("/models/{modelId}/predictions/{predictionId}/feedback")
//	public ResponseEntity<Prediction> getFeedback(@PathVariable Long modelId, @PathVariable Long predictionId, @Valid @RequestBody FeedbackResponseDTO feedbackResponseDto) {
//
//		ResponseEntity<Prediction> response = null;
//
//		if(predictionService.findOne(predictionId) == null) {
//			throw new BadRequestAlertException("Cannot find Model with the above prediction id", "Prediction", "Not Found");
//		}
//		if(feedbackResponseDto.getFeedbackValue() ==null) {
//			throw new BadRequestAlertException("Feedback Value cannot be null", "Prediction", "Null");
//
//		}
//		else {
//			log.debug("Prediction : ",predictionService.findOne(predictionId));
//			Prediction prediction = predictionService.findOne(predictionId);
//			prediction.setFeedbackClass(feedbackResponseDto.getFeedbackClass());
//			prediction.setFeedbackValue(feedbackResponseDto.getFeedbackValue());
//			prediction.setFeedbackFlag(true);
//			prediction.setFeedbackTimestamp(Instant.now());
//			prediction.setFeedbackUser(SecurityUtils.getCurrentUserLogin().get());
//			predictionService.save(prediction);
//			response = ResponseEntity.ok(prediction);
//			return response;
//		}
//	}
}
