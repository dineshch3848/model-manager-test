package com.predera.dmml.web.rest.vm;

public class PredictionTrendDTO {
    
    private Object timestamp;

    private int count;

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "PredictionTrendDTO [timestamp=" + timestamp + ", count=" + count + "]";
    }

}
