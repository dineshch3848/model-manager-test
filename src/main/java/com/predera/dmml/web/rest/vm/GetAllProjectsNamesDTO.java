package com.predera.dmml.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAllProjectsNamesDTO {
    
    @JsonProperty("project_name")
    private Object projectName;

    public Object getProjectName() {
        return projectName;
    }

    public void setProjectName(Object projectName) {
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return "GetAllProjectsNamesDTO [projectName=" + projectName + "]";
    }
    
}
