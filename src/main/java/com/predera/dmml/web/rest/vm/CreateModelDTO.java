package com.predera.dmml.web.rest.vm;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateModelDTO implements Serializable {

	/**
	 * CreateModelDTO
	 */

    private String id;
    
    @NotNull
	private String name;

	@NotNull
	private String type;

	@NotNull
	private String algorithm;
	
	@NotNull
	private String project;
	
	@NotNull
	private String version;

	@NotNull
	@JsonProperty("performance_metrics")
	private Map<String, Object> performanceMetrics;

	@NotNull
	@JsonProperty("feature_significance")
	private List<List<Object>> featureSignificance;

	@NotNull
	@JsonProperty("builder_config")
	private Map<String, Object> builderConfig;

	@NotNull
	@JsonProperty("training_dataset")
	private Map<String, Object> trainingDataset;
	
	@NotNull
	private String library;
	
	@JsonProperty("model_location")
	private String modelLocation;
	
	private String status;
	
	private String owner;
	
	@JsonProperty("created_date")
	private Date createdDate;
	
	@JsonProperty("deployed_date")
	private Date deployedDate;
	
	@JsonProperty("replication_factor")
	private String replicationFactor;
	
	@JsonProperty("prediction_url")
	private String predictionUrl;
	
	@JsonProperty("notification_value")
	private Boolean notificationValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Map<String, Object> getPerformanceMetrics() {
        return performanceMetrics;
    }

    public void setPerformanceMetrics(Map<String, Object> trainingDataset2) {
        this.performanceMetrics = trainingDataset2;
    }

    public List<List<Object>> getFeatureSignificance() {
        return featureSignificance;
    }

    public void setFeatureSignificance(List<List<Object>> featureSignificance) {
        this.featureSignificance = featureSignificance;
    }

    public Map<String, Object> getBuilderConfig() {
        return builderConfig;
    }

    public void setBuilderConfig(Map<String, Object> builderConfig) {
        this.builderConfig = builderConfig;
    }

    public Map<String, Object> getTrainingDataset() {
        return trainingDataset;
    }

    public void setTrainingDataset(Map<String, Object> trainingDataset) {
        this.trainingDataset = trainingDataset;
    }

    public String getLibrary() {
        return library;
    }

    public void setLibrary(String library) {
        this.library = library;
    }

    public String getModelLocation() {
        return modelLocation;
    }

    public void setModelLocation(String modelLocation) {
        this.modelLocation = modelLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getDeployedDate() {
        return deployedDate;
    }

    public void setDeployedDate(Date deployedDate) {
        this.deployedDate = deployedDate;
    }

    

    public String getReplicationFactor() {
        return replicationFactor;
    }

    public void setReplicationFactor(String replicationFactor) {
        this.replicationFactor = replicationFactor;
    }

    public String getPredictionUrl() {
        return predictionUrl;
    }

    public void setPredictionUrl(String predictionUrl) {
        this.predictionUrl = predictionUrl;
    }

    public Boolean getNotificationValue() {
        return notificationValue;
    }

    public void setNotificationValue(Boolean notificationValue) {
        this.notificationValue = notificationValue;
    }

    @Override
    public String toString() {
        return "CreateModelDTO [id=" + id + ", name=" + name + ", type=" + type + ", algorithm=" + algorithm
                + ", project=" + project + ", version=" + version + ", performanceMetrics=" + performanceMetrics
                + ", featureSignificance=" + featureSignificance + ", builderConfig=" + builderConfig
                + ", trainingDataset=" + trainingDataset + ", library=" + library + ", modelLocation=" + modelLocation
                + ", status=" + status + ", owner=" + owner + ", createdDate=" + createdDate + ", deployedDate="
                + deployedDate + ",replicationFactor="+replicationFactor+",predictionUrl="+predictionUrl+",notificationValue="+notificationValue+"]";
    }

//    public Map<String, Object> toMap() {
//        Map<String, Object> map = new HashMap<>();
//        map.put("name", name);
//        map.put("type",type);
//        map.put("algorithm", algorithm);
//        map.put("project", project);
//        map.put("version", version);
//        map.put("performance_metrics", performanceMetrics);
//        map.put("feature_significance", featureSignificance);
//        map.put("builder_config", builderConfig);
//        map.put("training_dataset", trainingDataset);
//        map.put("library", library);
//        map.put("model_location", modelLocation);
//        map.put("status", status);
//        map.put("owner", owner);
//        map.put("created_date", createdDate);
//        map.put("deployed_date", deployedDate);
//        map.put("replication_factor", replicationFactor);
//        map.put("prediction_url", predictionUrl);
//
//        return map;
//    }

    
}
