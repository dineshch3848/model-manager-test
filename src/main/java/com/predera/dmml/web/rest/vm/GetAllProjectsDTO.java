package com.predera.dmml.web.rest.vm;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAllProjectsDTO {

    private String project;
    
    @JsonProperty("no_of_models")
    private int numberOfModels;
    
    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public int getNumberOfModels() {
        return numberOfModels;
    }

    public void setNumberOfModels(int i) {
        this.numberOfModels = i;
    }

    @Override
    public String toString() {
        return "GetAllProjectsDTO [project=" + project + ", numberOfModels=" + numberOfModels + "]";
    }
}
