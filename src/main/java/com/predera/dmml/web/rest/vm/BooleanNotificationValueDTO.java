package com.predera.dmml.web.rest.vm;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BooleanNotificationValueDTO {

    @JsonProperty("notification_value")
    private boolean notificationValue;

    public boolean isNotificationValue() {
        return notificationValue;
    }

    public void setNotificationValue(boolean notificationValue) {
        this.notificationValue = notificationValue;
    }

    @Override
    public String toString() {
        return "BooleanNotificationValueDTO [notificationValue=" + notificationValue + "]";
    }
}
