package com.predera.dmml.web.rest.vm;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReplicationFactorDTO {

    @NotNull
    @JsonProperty("replication_factor")
    private String replicationFactor;

    public String getReplicationFactor() {
        return replicationFactor;
    }

    public void setReplicationFactor(String replicationFactor) {
        this.replicationFactor = replicationFactor;
    }

    @Override
    public String toString() {
        return "ReplicationFactorDTO [replicationFactor=" + replicationFactor + "]";
    }
    
    
    
}
