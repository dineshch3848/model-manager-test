package com.predera.dmml.service;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.predera.dmml.config.ApplicationProperties;
import com.predera.dmml.service.dto.ListOfHealthBucketsDTO;
import com.predera.dmml.service.dto.ListOfPredictionsBucketDTO;
import com.predera.dmml.service.dto.ListOfPredictionsDTO;
import com.predera.dmml.service.dto.PredictionDTO;
import com.predera.dmml.service.dto.PredictionResponseDTO;
import com.predera.dmml.web.rest.vm.CreateModelDTO;
import com.predera.dmml.web.rest.vm.ModelHealthDTO;
import com.predera.dmml.web.rest.vm.PredictionTrendDTO;

@Service
public class PredictionsDataService {
    private static final Logger log = LoggerFactory.getLogger(PredictionsDataService.class);

    private final String index;
    private final String docType;
    private final ObjectMapper mapper;
    private String healthQuery;
    
    private RestHighLevelClient client;
    
    RestClient restClient = RestClient.builder(
            new HttpHost("localhost", 9200, "http"),
            new HttpHost("localhost", 9201, "http")).build();

    public PredictionsDataService(RestHighLevelClient client, ApplicationProperties applicationProperties, ObjectMapper mapper) {
        this.client = client;
        this.mapper = mapper;
        this.index = applicationProperties.getDataService().getElasticsearch().getPredictions().getIndex();
        this.docType = applicationProperties.getDataService().getElasticsearch().getPredictions().getDocType();
        this.healthQuery = applicationProperties.getQueries().getHealthQuery();
    }

    public PredictionDTO savePrediction(PredictionDTO prediction) throws IOException {
        log.debug("Request to save Prediction: {}", prediction);
        IndexRequest indexRequest = new IndexRequest(prediction.getIndex(), prediction.getType());
        indexRequest.source(prediction.toMap());
        IndexResponse response = client.index(indexRequest);
        log.debug("Index response: {}", response);

        prediction.setId(response.getId());
        return prediction;
    }

    @Async
    public CompletableFuture<PredictionDTO> asyncSavePrediction(CreateModelDTO model, Map<String, Object> jsonInput,
                                                                PredictionResponseDTO predictionResponseDTO) throws IOException {
        return asyncSavePrediction(model, jsonInput, predictionResponseDTO, null);
    }

    @Async
    public CompletableFuture<PredictionDTO> asyncSavePrediction(CreateModelDTO model, Map<String, Object> jsonInput,
                                                                PredictionResponseDTO predictionResponseDTO,
                                                                String uid) throws IOException {
    	PredictionDTO predictionDTO = new PredictionDTO();
        predictionDTO.setIndex(index);
        predictionDTO.setType(docType);
        predictionDTO.setModelId(model.getId());
        predictionDTO.setModelName(model.getName());
        predictionDTO.setProject(model.getProject());
        predictionDTO.setVersion(model.getVersion());
        predictionDTO.setUid(uid);
        predictionDTO.setPredictionInput(jsonInput);
        predictionDTO.setPredictionDate(Date.from(Instant.now()));
        predictionDTO.setPredictionOutput(predictionResponseDTO.getOutput().getResult());
        predictionDTO.setPredictionReasons(predictionResponseDTO.getOutput().getReasons());
        predictionDTO = savePrediction(predictionDTO);
        return CompletableFuture.completedFuture(predictionDTO);
    }

    public List<PredictionDTO> getPredictionsById(String id) throws IOException {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder(); 
        sourceBuilder.from(0);
        sourceBuilder.query(QueryBuilders.matchQuery("model_id", id));
        sourceBuilder.size(50);
        sourceBuilder.timeout(new TimeValue(120, TimeUnit.SECONDS));
        
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.source(sourceBuilder);
        searchRequest.types(docType);
        
        SearchResponse searchResponse = client.search(searchRequest);
        
        log.debug("search response {} ", searchResponse);
        
        Map<String, Object> list = mapper.readValue(mapper.writeValueAsString(searchResponse), Map.class);
        
        ListOfPredictionsDTO listOfPredictionsDTO = mapper.convertValue(list, ListOfPredictionsDTO.class);

        List<PredictionDTO> arrayList = new ArrayList<>();
        
        for(int i=0;i<listOfPredictionsDTO.getHits().getHits().size();i++) {
        PredictionDTO getAllPredictions = new PredictionDTO();
        getAllPredictions.setId(listOfPredictionsDTO.getHits().getHits().get(i).getId());
        getAllPredictions.setIndex(index);
        getAllPredictions.setType(docType);
        getAllPredictions.setModelId(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getModelId());
        getAllPredictions.setModelName(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getModelName());
        getAllPredictions.setProject(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getProject());
        getAllPredictions.setVersion(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getVersion());
        getAllPredictions.setUid(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getUid());
        getAllPredictions.setPredictionInput(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionInput());
        getAllPredictions.setPredictionDate(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionDate());
        getAllPredictions.setPredictionOutput(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionOutput());
        getAllPredictions.setPredictionReasons(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionReasons());
        arrayList.add(getAllPredictions);
        }
        return arrayList;
    }
    
    public List<PredictionDTO> getFeedbacksById(String id) throws IOException {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchQuery("model_id", id));
        sourceBuilder.query(QueryBuilders.existsQuery("feedback_value"));
        sourceBuilder.from(0);
        sourceBuilder.size(5000);
        sourceBuilder.timeout(new TimeValue(120, TimeUnit.SECONDS));
        
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.source(sourceBuilder);
        searchRequest.types(docType);
        
        SearchResponse searchResponse = client.search(searchRequest);
        
        log.debug("search response {} ", searchResponse);
        
        Map<String, Object> list = mapper.readValue(mapper.writeValueAsString(searchResponse), Map.class);
        
        ListOfPredictionsDTO listOfPredictionsDTO = mapper.convertValue(list, ListOfPredictionsDTO.class);

        List<PredictionDTO> arrayList = new ArrayList<>();
        
        for(int i=0;i<searchResponse.getHits().getTotalHits();i++) {
        PredictionDTO getAllPredictions = new PredictionDTO();
        getAllPredictions.setId(listOfPredictionsDTO.getHits().getHits().get(i).getId());
        getAllPredictions.setIndex(index);
        getAllPredictions.setType(docType);
        getAllPredictions.setModelId(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getModelId());
        getAllPredictions.setModelName(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getModelName());
        getAllPredictions.setProject(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getProject());
        getAllPredictions.setVersion(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getVersion());
        getAllPredictions.setUid(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getUid());
        getAllPredictions.setPredictionInput(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionInput());
        getAllPredictions.setPredictionDate(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionDate());
        getAllPredictions.setPredictionOutput(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionOutput());
        getAllPredictions.setPredictionReasons(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getPredictionReasons());
        getAllPredictions.setFeedbackTimestamp(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getFeedbackTimestamp());
        getAllPredictions.setFeedbackUser(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getFeedbackUser());
        getAllPredictions.setFeedbackValue(listOfPredictionsDTO.getHits().getHits().get(i).getSource().getFeedbackValue());
        arrayList.add(getAllPredictions);
        }
        return arrayList;
    } 

    public PredictionDTO getPredictionById(String id) throws IOException {
        GetRequest request = new GetRequest(index, docType, id);
        GetResponse response = client.get(request);
        PredictionDTO predictionDTO = new PredictionDTO();
        
        log.debug("response {}", response);
        
        Map<String, Object> source = response.getSource();
        
        log.debug("Source after map {}", source);
        
        predictionDTO.setId(response.getId());
        predictionDTO.setIndex(index);
        predictionDTO.setType(docType);
        predictionDTO.setModelId((String)source.get("model_id"));
        predictionDTO.setModelName((String)source.get("model_name"));
        predictionDTO.setProject((String)source.get("project"));
        predictionDTO.setVersion((String)source.get("version"));
        predictionDTO.setUid((String)source.get("uid"));
        predictionDTO.setPredictionInput((Map<String, Object>)source.get("prediction_input"));
        predictionDTO.setPredictionDate(new DateTime((String)source.get("prediction_date")).toDate());
        predictionDTO.setPredictionOutput((Object)source.get("prediction_output"));
        predictionDTO.setPredictionReasons((Map<String, Object>)source.get("prediction_reasons"));
        return predictionDTO;
    }

    public List<ModelHealthDTO> getModelHealth(String id) throws IOException {
        log.debug("into get Health method");
        
        Map<String, String> params = Collections.emptyMap();
        String jsonString = "{\"size\": 0,\"query\": {\"match\": {\"model_id\": \""+id+"\"}},\"aggs\": {\"date_buckets\": {\"date_histogram\": {\"field\": \"prediction_date\",\"interval\": \"1h\",\"min_doc_count\": 1},\"aggs\": {\"prediction_buckets\": {\"range\": {\"field\": \"prediction_output\",\"ranges\": [{\"key\": \"neg_predictions\",\"to\": 0.1}, {\"key\": \"pos_predictions\",\"from\": 0.9}]}}}}}}";
        HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
        Response response = restClient.performRequest("GET", "/predictions/_doc/_search", params, entity);;
        String responseBody = EntityUtils.toString(response.getEntity());
        
        ListOfHealthBucketsDTO listOfHealthbucketsDTO = mapper.readValue(responseBody, ListOfHealthBucketsDTO.class);
        
        List<ModelHealthDTO> arrayList = new ArrayList<>();
        
        for(int i=0;i<listOfHealthbucketsDTO.getAggregations().getDate_buckets().getBuckets().size();i++) {
        ModelHealthDTO modelHealthDTO = new ModelHealthDTO();
        modelHealthDTO.setTimestamp(listOfHealthbucketsDTO.getAggregations().getDate_buckets().getBuckets().get(i).getKey());
        int totalPredictions = listOfHealthbucketsDTO.getAggregations().getDate_buckets().getBuckets().get(i).getDoc_count();
        int negativePredictions = 0;
        int positivePredictions = 0;
        int healthPercentage = 0;
        for(int j=0; j<listOfHealthbucketsDTO.getAggregations().getDate_buckets().getBuckets().get(i).getPrediction_buckets().getBuckets().size()-1; j++) {
        negativePredictions = listOfHealthbucketsDTO.getAggregations().getDate_buckets().getBuckets().get(i).getPrediction_buckets().getBuckets().get(j).getDoc_count();
        positivePredictions = listOfHealthbucketsDTO.getAggregations().getDate_buckets().getBuckets().get(i).getPrediction_buckets().getBuckets().get(j+1).getDoc_count();
        healthPercentage = ((negativePredictions + positivePredictions)/totalPredictions)*100;
        }
        modelHealthDTO.setHealth(healthPercentage);
        arrayList.add(modelHealthDTO);
        }
        return arrayList;
    }

    public List<PredictionTrendDTO> getPredictionTrend(String id) throws IOException {
        log.debug("into get prediction trend method");
        
        Map<String, String> params = Collections.emptyMap();
        String jsonString = "{\"size\":0,\"query\":{\"match\":{\"model_id\":\""+id+"\"}},\"aggs\":{\"predictions_count_by_date\":{\"date_histogram\":{\"field\":\"prediction_date\",\"interval\":\"1h\",\"min_doc_count\":1}}}}";
        HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
        Response response = restClient.performRequest("GET", "/predictions/_doc/_search", params, entity);;
        String responseBody = EntityUtils.toString(response.getEntity());
        
        ListOfPredictionsBucketDTO listOfPredictionsBucketDTO = mapper.readValue(responseBody, ListOfPredictionsBucketDTO.class);
        
        List<PredictionTrendDTO> arrayList = new ArrayList<>();
        
        for(int i=0;i<listOfPredictionsBucketDTO.getAggregations().getPredictions_count_by_date().getBuckets().size();i++) {
        PredictionTrendDTO predictionTrendDTO = new PredictionTrendDTO();
        predictionTrendDTO.setTimestamp(listOfPredictionsBucketDTO.getAggregations().getPredictions_count_by_date().getBuckets().get(i).getKey());
        predictionTrendDTO.setCount(listOfPredictionsBucketDTO.getAggregations().getPredictions_count_by_date().getBuckets().get(i).getDoc_count());
        arrayList.add(predictionTrendDTO);
        }
        return arrayList;
    }
}
