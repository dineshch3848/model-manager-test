package com.predera.dmml.service.dto;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListOfPredictionsDTO {
    @JsonProperty("hits")
    private Hits hits;

    @JsonProperty("hits")
    public Hits getHits() {
        return hits;
    }

    @JsonProperty("hits")
    public void setHits(Hits hits) {
        this.hits = hits;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("hits", hits).toString();
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Hits 
    {

        @JsonProperty("hits")
        private List<Hit> hits;

        @JsonProperty("hits")
        public List<Hit> getHits() {
            return hits;
        }

        @JsonProperty("hits")
        public void setHits(List<Hit> hits) {
            this.hits = hits;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("hits", hits).toString();
        }
        
        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Hit 
        {

            @JsonProperty("id")
            private String id;
            @JsonProperty("sourceAsMap")
            private Source source;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }


            public Source getSource() {
                return source;
            }

            public void setSource(Source source) {
                this.source = source;
            }


            @Override
            public String toString() {
                return "Hit [id=" + id + ", source=" + source + "]";
            }

            @JsonIgnoreProperties(ignoreUnknown = true)
            public static class Source 
            {
                @JsonProperty("uid")
                private String uid;
                @JsonProperty("prediction_date")
                private Date predictionDate;
                @JsonProperty("model_name")
                private String modelName;
                @JsonProperty("prediction_output")
                private Object predictionOutput;
                @JsonProperty("project")
                private String project;
                @JsonProperty("prediction_input")
                private Map<String, Object> predictionInput;
                @JsonProperty("prediction_reasons")
                private Map<String, Object> predictionReasons;
                @JsonProperty("model_id")
                private String modelId;
                @JsonProperty("version")
                private String version;
                @JsonProperty("feedback_timestamp")
                private Date feedbackTimestamp;
                @JsonProperty("feedback_user")
                private String feedbackUser;
                @JsonProperty("feedback_value")
                private String feedbackValue;
                
                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public Date getPredictionDate() {
                    return predictionDate;
                }

                public void setPredictionDate(Date predictionDate) {
                    this.predictionDate = predictionDate;
                }

                public String getModelName() {
                    return modelName;
                }

                public void setModelName(String modelName) {
                    this.modelName = modelName;
                }

                public Object getPredictionOutput() {
                    return predictionOutput;
                }

                public void setPredictionOutput(Object predictionOutput) {
                    this.predictionOutput = predictionOutput;
                }

                public String getProject() {
                    return project;
                }

                public void setProject(String project) {
                    this.project = project;
                }

                public Map<String, Object> getPredictionInput() {
                    return predictionInput;
                }

                public void setPredictionInput(Map<String, Object> predictionInput) {
                    this.predictionInput = predictionInput;
                }

                public Map<String, Object> getPredictionReasons() {
                    return predictionReasons;
                }

                public void setPredictionReasons(Map<String, Object> predictionReasons) {
                    this.predictionReasons = predictionReasons;
                }

                public String getModelId() {
                    return modelId;
                }

                public void setModelId(String modelId) {
                    this.modelId = modelId;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public Date getFeedbackTimestamp() {
                    return feedbackTimestamp;
                }

                public void setFeedbackTimestamp(Date feedbackTimestamp) {
                    this.feedbackTimestamp = feedbackTimestamp;
                }

                public String getFeedbackUser() {
                    return feedbackUser;
                }

                public void setFeedbackUser(String feedbackUser) {
                    this.feedbackUser = feedbackUser;
                }

                public String getFeedbackValue() {
                    return feedbackValue;
                }

                public void setFeedbackValue(String feedbackValue) {
                    this.feedbackValue = feedbackValue;
                }

                @Override
                public String toString() {
                    return "Source [uid=" + uid + ", predictionDate=" + predictionDate + ", modelName=" + modelName
                            + ", predictionOutput=" + predictionOutput + ", project=" + project + ", predictionInput="
                            + predictionInput + ", predictionReasons=" + predictionReasons + ", modelId=" + modelId
                            + ", version=" + version + ", feedbackTimestamp=" + feedbackTimestamp + ", feedbackUser="
                            + feedbackUser + ", feedbackValue=" + feedbackValue + "]";
                }

                
            }
        }

    }
}
