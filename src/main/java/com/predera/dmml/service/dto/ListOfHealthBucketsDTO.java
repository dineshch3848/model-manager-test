package com.predera.dmml.service.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.logstash.logback.encoder.org.apache.commons.lang.builder.ToStringBuilder;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListOfHealthBucketsDTO {

    @JsonProperty("aggregations")
    private Aggregations aggregations;
    private final static long serialVersionUID = 8695493699456461708L;

    @JsonProperty("aggregations")
    public Aggregations getAggregations() {
        return aggregations;
    }

    @JsonProperty("aggregations")
    public void setAggregations(Aggregations aggregations) {
        this.aggregations = aggregations;
    }

    @Override
    public String toString() {
        return "ListOfHealthbucketsDTO [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + "]";
    }

    public static class Aggregations implements Serializable {

        @JsonProperty("date_buckets")
        private Date_buckets date_buckets;
        private final static long serialVersionUID = 4954186528379390896L;

        @JsonProperty("date_buckets")
        public Date_buckets getDate_buckets() {
            return date_buckets;
        }

        @JsonProperty("date_buckets")
        public void setDate_buckets(Date_buckets date_buckets) {
            this.date_buckets = date_buckets;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("date_buckets", date_buckets).toString();
        }
    }

    public static class Date_buckets implements Serializable {

        @JsonProperty("buckets")
        private List<Bucket> buckets = null;
        private final static long serialVersionUID = 6129226504784548772L;

        @JsonProperty("buckets")
        public List<Bucket> getBuckets() {
            return buckets;
        }

        @JsonProperty("buckets")
        public void setBuckets(List<Bucket> buckets) {
            this.buckets = buckets;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("buckets", buckets).toString();
        }

        public static class Bucket implements Serializable {

            @JsonProperty("key_as_string")
            private String key_as_string;
            @JsonProperty("key")
            private Object key;
            @JsonProperty("doc_count")
            private Integer doc_count;
            @JsonProperty("prediction_buckets")
            private Prediction_buckets prediction_buckets;
            private final static long serialVersionUID = 1507739638460544152L;

            @JsonProperty("key_as_string")
            public String getKey_as_string() {
                return key_as_string;
            }

            @JsonProperty("key_as_string")
            public void setKey_as_string(String key_as_string) {
                this.key_as_string = key_as_string;
            }

            @JsonProperty("key")
            public Object getKey() {
                return key;
            }

            @JsonProperty("key")
            public void setKey(Object key) {
                this.key = key;
            }

            @JsonProperty("doc_count")
            public Integer getDoc_count() {
                return doc_count;
            }

            @JsonProperty("doc_count")
            public void setDoc_count(Integer doc_count) {
                this.doc_count = doc_count;
            }

            @JsonProperty("prediction_buckets")
            public Prediction_buckets getPrediction_buckets() {
                return prediction_buckets;
            }

            @JsonProperty("prediction_buckets")
            public void setPrediction_buckets(Prediction_buckets prediction_buckets) {
                this.prediction_buckets = prediction_buckets;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("key_as_string", key_as_string).append("key", key)
                        .append("doc_count", doc_count).append("prediction_buckets", prediction_buckets).toString();
            }

            public static class Prediction_buckets implements Serializable {

                @JsonProperty("buckets")
                private List<Bucket_> buckets = null;
                private final static long serialVersionUID = 4141185363569778011L;

                @JsonProperty("buckets")
                public List<Bucket_> getBuckets() {
                    return buckets;
                }

                @JsonProperty("buckets")
                public void setBuckets(List<Bucket_> buckets) {
                    this.buckets = buckets;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("buckets", buckets).toString();
                }
            }

            public static class Bucket_ implements Serializable {

                @JsonProperty("key")
                private String key;
                @JsonProperty("to")
                private Float to;
                @JsonProperty("doc_count")
                private Integer doc_count;
                @JsonProperty("from")
                private Float from;
                private final static long serialVersionUID = 4230256793112426367L;

                @JsonProperty("key")
                public String getKey() {
                    return key;
                }

                @JsonProperty("key")
                public void setKey(String key) {
                    this.key = key;
                }

                @JsonProperty("to")
                public Float getTo() {
                    return to;
                }

                @JsonProperty("to")
                public void setTo(Float to) {
                    this.to = to;
                }

                @JsonProperty("doc_count")
                public Integer getDoc_count() {
                    return doc_count;
                }

                @JsonProperty("doc_count")
                public void setDoc_count(Integer doc_count) {
                    this.doc_count = doc_count;
                }

                @JsonProperty("from")
                public Float getFrom() {
                    return from;
                }

                @JsonProperty("from")
                public void setFrom(Float from) {
                    this.from = from;
                }

                @Override
                public String toString() {
                    return new ToStringBuilder(this).append("key", key).append("to", to).append("doc_count", doc_count)
                            .append("from", from).toString();
                }
            }
        }
    }
}
