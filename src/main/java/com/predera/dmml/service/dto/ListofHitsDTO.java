package com.predera.dmml.service.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListofHitsDTO {
    @JsonProperty("hits")
    private Hits hits;

    @JsonProperty("hits")
    public Hits getHits() {
        return hits;
    }

    @JsonProperty("hits")
    public void setHits(Hits hits) {
        this.hits = hits;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("hits", hits).toString();
    }


    
    public static class Hits 
    {

        @JsonProperty("hits")
        private List<Hit> hits;

        @JsonProperty("hits")
        public List<Hit> getHits() {
            return hits;
        }

        @JsonProperty("hits")
        public void setHits(List<Hit> hits) {
            this.hits = hits;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("hits", hits).toString();
        }
        

        public static class Hit 
        {

            @JsonProperty("_id")
            private String id;
            @JsonProperty("_source")
            private Source source;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }


            public Source getSource() {
                return source;
            }

            public void setSource(Source source) {
                this.source = source;
            }


            @Override
            public String toString() {
                return "Hit [id=" + id + ", source=" + source + "]";
            }


            public static class Source 
            {

                @JsonProperty("id")
                private Object id;
                @JsonProperty("name")
                private String name;
                @JsonProperty("type")
                private String type;
                @JsonProperty("algorithm")
                private String algorithm;
                @JsonProperty("project")
                private String project;
                @JsonProperty("version")
                private String version;
                @JsonProperty("status")
                private String status;
                @JsonProperty("owner")
                private Object owner;

                @JsonProperty("performance_metrics")
                private Map<String, Object> performanceMetrics;

                @JsonProperty("feature_significance")
                private List<List<Object>> featureSignificance;

                @JsonProperty("library")
                private String library;
                
                @JsonProperty("created_date")
                private Date createdDate;

                public Object getId() {
                    return id;
                }

                public void setId(Object id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getAlgorithm() {
                    return algorithm;
                }

                public void setAlgorithm(String algorithm) {
                    this.algorithm = algorithm;
                }

                public String getProject() {
                    return project;
                }

                public void setProject(String project) {
                    this.project = project;
                }

                public String getVersion() {
                    return version;
                }

                public void setVersion(String version) {
                    this.version = version;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public Object getOwner() {
                    return owner;
                }

                public void setOwner(Object owner) {
                    this.owner = owner;
                }

                public Map<String, Object> getPerformanceMetrics() {
                    return performanceMetrics;
                }

                public void setPerformanceMetrics(Map<String, Object> performanceMetrics) {
                    this.performanceMetrics = performanceMetrics;
                }

                public List<List<Object>> getFeatureSignificance() {
                    return featureSignificance;
                }

                public void setFeatureSignificance(List<List<Object>> featureSignificance) {
                    this.featureSignificance = featureSignificance;
                }

                public String getLibrary() {
                    return library;
                }

                public void setLibrary(String library) {
                    this.library = library;
                }

                public Date getCreatedDate() {
                    return createdDate;
                }

                public void setCreatedDate(Date createdDate) {
                    this.createdDate = createdDate;
                }

                @Override
                public String toString() {
                    return "Source [id=" + id + ", name=" + name + ", type=" + type + ", algorithm=" + algorithm
                            + ", project=" + project + ", version=" + version + ", status=" + status + ", owner="
                            + owner + ", performanceMetrics=" + performanceMetrics + ", featureSignificance="
                            + featureSignificance + ", library=" + library + ", createdDate=" + createdDate + "]";
                }

                
            }
        }

    }
}
