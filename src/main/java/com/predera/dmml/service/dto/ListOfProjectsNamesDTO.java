package com.predera.dmml.service.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.logstash.logback.encoder.org.apache.commons.lang.builder.ToStringBuilder;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListOfProjectsNamesDTO {
    @JsonProperty("aggregations")
    private Aggregations aggregations;
    private final static long serialVersionUID = 8695493699456461708L;

    @JsonProperty("aggregations")
    public Aggregations getAggregations() {
        return aggregations;
    }

    @JsonProperty("aggregations")
    public void setAggregations(Aggregations aggregations) {
        this.aggregations = aggregations;
    }

    @Override
    public String toString() {
        return "ListOfHealthbucketsDTO [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
                + super.toString() + "]";
    }

    public static class Aggregations implements Serializable {

        @JsonProperty("unique_projects")
        private Unique_Projects unique_projects;
        private final static long serialVersionUID = 4954186528379390896L;

        public Unique_Projects getUnique_projects() {
            return unique_projects;
        }

        public void setUnique_projects(Unique_Projects unique_projects) {
            this.unique_projects = unique_projects;
        }

        @Override
        public String toString() {
            return "Aggregations [unique_projects=" + unique_projects + "]";
        }

    }

    public static class Unique_Projects implements Serializable {

        @JsonProperty("buckets")
        private List<Bucket> buckets = null;
        private final static long serialVersionUID = 6129226504784548772L;

        @JsonProperty("buckets")
        public List<Bucket> getBuckets() {
            return buckets;
        }

        @JsonProperty("buckets")
        public void setBuckets(List<Bucket> buckets) {
            this.buckets = buckets;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("buckets", buckets).toString();
        }

        public static class Bucket implements Serializable {

            @JsonProperty("key")
            private Object key;
            @JsonProperty("doc_count")
            private Integer doc_count;

            private final static long serialVersionUID = 1507739638460544152L;

            @JsonProperty("key")
            public Object getKey() {
                return key;
            }

            @JsonProperty("key")
            public void setKey(Object key) {
                this.key = key;
            }

            @JsonProperty("doc_count")
            public Integer getDoc_count() {
                return doc_count;
            }

            @JsonProperty("doc_count")
            public void setDoc_count(Integer doc_count) {
                this.doc_count = doc_count;
            }

            @Override
            public String toString() {
                return "Bucket [key=" + key + ", doc_count=" + doc_count + "]";
            }

        }
    }
}
