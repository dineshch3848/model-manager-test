package com.predera.dmml.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.predera.dmml.service.PredictionFeedbackDTO;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PredictionDTO {

    private String id;

    @NotNull
    private String index;

    @NotNull
    private String type;

    @NotNull
    @JsonProperty("model_id")
    private String modelId;

    @NotNull
    @JsonProperty("model_name")
    private String modelName;

    @NotNull
    private String project;

    @NotNull
    private String version;

    private String uid;

    @NotNull
    @JsonProperty("prediction_input")
    private Map<String, Object> predictionInput;

    @NotNull
    @JsonProperty("prediction_date")
    private Date predictionDate;

    @NotNull
    @JsonProperty("prediction_output")
    private Object predictionOutput;

    @JsonProperty("prediction_reasons")
    private Map<String, Object> predictionReasons;

    @JsonProperty("feedback_timestamp")
    private Date feedbackTimestamp;
    
    @JsonProperty("feedback_user")
    private String feedbackUser;
    
    @JsonProperty("feedback_value")
    private String feedbackValue;
    

    public PredictionDTO() {
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
//        map.put("index", index);
//        map.put("type", type);
        map.put("model_id", modelId);
        map.put("model_name", modelName);
        map.put("project", project);
        map.put("version", version);
        map.put("uid", uid);
        map.put("prediction_input", predictionInput);
//        for (String key : predictionInput.keySet()) {
//            map.put(key, predictionInput.get(key));
//        }
        map.put("prediction_date", predictionDate);
        map.put("prediction_output", predictionOutput);
        map.put("prediction_reasons", predictionReasons);
        return map;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String string) {
        this.modelId = string;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Map<String, Object> getPredictionInput() {
        return predictionInput;
    }

    public void setPredictionInput(Map<String, Object> jsonInput) {
        this.predictionInput = jsonInput;
    }

    public Date getPredictionDate() {
        return predictionDate;
    }

    public void setPredictionDate(Date predictionDate) {
        this.predictionDate = predictionDate;
    }

    public Object getPredictionOutput() {
        return predictionOutput;
    }

    public void setPredictionOutput(Object predictionOutput) {
        this.predictionOutput = predictionOutput;
    }

    public Map<String, Object> getPredictionReasons() {
        return predictionReasons;
    }

    public void setPredictionReasons(Map<String, Object> predictionReasons) {
        this.predictionReasons = predictionReasons;
    }

    public Date getFeedbackTimestamp() {
        return feedbackTimestamp;
    }

    public void setFeedbackTimestamp(Date feedbackTimestamp) {
        this.feedbackTimestamp = feedbackTimestamp;
    }

    public String getFeedbackUser() {
        return feedbackUser;
    }

    public void setFeedbackUser(String feedbackUser) {
        this.feedbackUser = feedbackUser;
    }

    public String getFeedbackValue() {
        return feedbackValue;
    }

    public void setFeedbackValue(String feedbackValue) {
        this.feedbackValue = feedbackValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PredictionDTO that = (PredictionDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(index, that.index) &&
            Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, index, type);
    }

    @Override
    public String toString() {
        return "PredictionDTO [id=" + id + ", index=" + index + ", type=" + type + ", modelId=" + modelId
                + ", modelName=" + modelName + ", project=" + project + ", version=" + version + ", uid=" + uid
                + ", predictionInput=" + predictionInput + ", predictionDate=" + predictionDate + ", predictionOutput="
                + predictionOutput + ", predictionReasons=" + predictionReasons + ", feedbackTimestamp="
                + feedbackTimestamp + ", feedbackUser=" + feedbackUser + ", feedbackValue=" + feedbackValue + "]";
    }

    
}
