package com.predera.dmml.service.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetAllModelsDTO {
    
    private Object id;
    
    private String name;

    private String type;

    private String algorithm;

    private String project;

    private String version;

    private String status;

    private String owner;
    
    @JsonProperty("count_of_features")
    private int countOfFeatures;
    
    private Object auc;
    
    private Object accuracy;
    
    private Object precision;
    
    private Object recall;
    
    private String library;
    
    @JsonProperty("created_date")
    private Date createdDate;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getCountOfFeatures() {
        return countOfFeatures;
    }

    public void setCountOfFeatures(int countOfFeatures) {
        this.countOfFeatures = countOfFeatures;
    }

    public Object getAuc() {
        return auc;
    }

    public void setAuc(Object object) {
        this.auc = object;
    }

    public Object getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Object accuracy) {
        this.accuracy = accuracy;
    }

    public Object getPrecision() {
        return precision;
    }

    public void setPrecision(Object precision) {
        this.precision = precision;
    }

    public Object getRecall() {
        return recall;
    }

    public void setRecall(Object recall) {
        this.recall = recall;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLibrary() {
        return library;
    }

    public void setLibrary(String library) {
        this.library = library;
    }

    @Override
    public String toString() {
        return "GetAllModelsDTO [id=" + id + ", name=" + name + ", type=" + type + ", algorithm=" + algorithm
                + ", project=" + project + ", version=" + version + ", status=" + status + ", owner=" + owner
                + ", countOfFeatures=" + countOfFeatures + ", auc=" + auc + ", accuracy=" + accuracy + ", precision="
                + precision + ", recall=" + recall + ", createdDate=" + createdDate + "]";
    }
}
