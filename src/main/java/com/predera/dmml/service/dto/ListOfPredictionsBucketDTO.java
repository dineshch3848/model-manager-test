package com.predera.dmml.service.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.logstash.logback.encoder.org.apache.commons.lang.builder.ToStringBuilder;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListOfPredictionsBucketDTO {

    @JsonProperty("aggregations")
    private Aggregations aggregations;
    private final static long serialVersionUID = 8695493699456461708L;

    @JsonProperty("aggregations")
    public Aggregations getAggregations() {
        return aggregations;
    }

    @JsonProperty("aggregations")
    public void setAggregations(Aggregations aggregations) {
        this.aggregations = aggregations;
    }

    @Override
    public String toString() {
        return "Example [aggregations=" + aggregations + "]";
    }

    public static class Aggregations implements Serializable {

        @JsonProperty("predictions_count_by_date")
        private Predictions_count_by_date predictions_count_by_date;
        private final static long serialVersionUID = 7205117016994707412L;

        @JsonProperty("predictions_count_by_date")
        public Predictions_count_by_date getPredictions_count_by_date() {
            return predictions_count_by_date;
        }

        @JsonProperty("predictions_count_by_date")
        public void setPredictions_count_by_date(Predictions_count_by_date predictions_count_by_date) {
            this.predictions_count_by_date = predictions_count_by_date;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("predictions_count_by_date", predictions_count_by_date).toString();
        }

    }

    public static class Predictions_count_by_date implements Serializable {

        @JsonProperty("buckets")
        private List<Bucket> buckets = null;
        private final static long serialVersionUID = 2659322706502871374L;

        @JsonProperty("buckets")
        public List<Bucket> getBuckets() {
            return buckets;
        }

        @JsonProperty("buckets")
        public void setBuckets(List<Bucket> buckets) {
            this.buckets = buckets;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("buckets", buckets).toString();
        }

    }

    public static class Bucket implements Serializable {

        @JsonProperty("key_as_string")
        private String key_as_string;
        @JsonProperty("key")
        private Object key;
        @JsonProperty("doc_count")
        private Integer doc_count;
        private final static long serialVersionUID = -7439461819440198034L;

        @JsonProperty("key_as_string")
        public String getKey_as_string() {
            return key_as_string;
        }

        @JsonProperty("key_as_string")
        public void setKey_as_string(String key_as_string) {
            this.key_as_string = key_as_string;
        }

        @JsonProperty("key")
        public Object getKey() {
            return key;
        }

        @JsonProperty("key")
        public void setKey(Object key) {
            this.key = key;
        }

        @JsonProperty("doc_count")
        public Integer getDoc_count() {
            return doc_count;
        }

        @JsonProperty("doc_count")
        public void setDoc_count(Integer doc_count) {
            this.doc_count = doc_count;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("key_as_string", key_as_string).append("key", key)
                    .append("doc_count", doc_count).toString();
        }

    }
}
