package com.predera.dmml.service;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.DocWriteResponse.Result;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.predera.dmml.config.ApplicationProperties;
import com.predera.dmml.security.SecurityUtils;
import com.predera.dmml.service.dto.GetAllModelsDTO;
import com.predera.dmml.service.dto.ListOfPredictionsBucketDTO;
import com.predera.dmml.service.dto.ListOfProjectsNamesDTO;
import com.predera.dmml.service.dto.ListofHitsDTO;
import com.predera.dmml.web.rest.vm.CreateModelDTO;
import com.predera.dmml.web.rest.vm.GetAllProjectsDTO;
import com.predera.dmml.web.rest.vm.GetAllProjectsNamesDTO;
import com.predera.dmml.web.rest.vm.ModelHealthDTO;
import com.predera.dmml.web.rest.vm.PredictionTrendDTO;

@Service
public class ModelsDataService {
    private static final Logger log = LoggerFactory.getLogger(PredictionsDataService.class);

    private final String index;
    private final String docType;
    private final ObjectMapper mapper;
    
    private RestHighLevelClient client;
    
    RestClient restClient = RestClient.builder(
            new HttpHost("localhost", 9200, "http"),
            new HttpHost("localhost", 9201, "http")).setMaxRetryTimeoutMillis(120000).build();

    public ModelsDataService(RestHighLevelClient client, ApplicationProperties applicationProperties, ObjectMapper mapper) {
        this.client = client;
        this.mapper = mapper;
        this.index = applicationProperties.getDataService().getElasticsearch().getModels().getIndex();
        this.docType = applicationProperties.getDataService().getElasticsearch().getModels().getDocType();
    }
    
    public CreateModelDTO saveModel(CreateModelDTO inputModel, String loginUser) throws IOException {
        
        CreateModelDTO modelDTO = new CreateModelDTO();
        modelDTO.setName(inputModel.getName());
        modelDTO.setType(inputModel.getType());
        modelDTO.setAlgorithm(inputModel.getAlgorithm());
        modelDTO.setProject(inputModel.getProject());
        modelDTO.setVersion(inputModel.getVersion());
        modelDTO.setPerformanceMetrics(mapper.readValue(mapper.writeValueAsString(inputModel.getPerformanceMetrics()), Map.class));
        modelDTO.setBuilderConfig(mapper.readValue(mapper.writeValueAsString(inputModel.getBuilderConfig()), Map.class));
        modelDTO.setTrainingDataset(mapper.readValue(mapper.writeValueAsString(inputModel.getTrainingDataset()), Map.class));
        modelDTO.setFeatureSignificance(mapper.readValue(mapper.writeValueAsString(inputModel.getFeatureSignificance()), List.class));
        modelDTO.setLibrary(inputModel.getLibrary());
        modelDTO.setStatus("Experiment");
        modelDTO.setOwner(loginUser);
        modelDTO.setCreatedDate(Date.from(Instant.now()));
        modelDTO.setNotificationValue(false);
        
        String json = mapper.writeValueAsString(modelDTO);
        Map<String, Object> map = mapper.readValue(json, Map.class);

        IndexRequest indexRequest = new IndexRequest(index, docType);
        indexRequest.source(map);
        IndexResponse response = client.index(indexRequest);
        modelDTO.setId(response.getId());
        updateModelId(response.getId());
        return modelDTO;
    }

    private void updateModelId(String id) throws IOException {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("id", id);
        UpdateRequest request = new UpdateRequest(index,docType,id)
        .doc(jsonMap);
        client.update(request);
    }

    public CreateModelDTO getModelById(String id) throws IOException {
        GetRequest request = new GetRequest(index, docType, id);
        GetResponse response = client.get(request);
        CreateModelDTO modelDTO = new CreateModelDTO();
        log.debug("response {}", response);
        
        Map<String, Object> source = response.getSource();
        
        log.debug("Source after map {}", source);
        
        modelDTO.setId(response.getId());       
        modelDTO.setName((String)source.get("name"));
        modelDTO.setType((String)source.get("type"));
        modelDTO.setAlgorithm((String)source.get("algorithm"));
        modelDTO.setProject((String)source.get("project"));
        modelDTO.setVersion((String)source.get("version"));
        modelDTO.setPerformanceMetrics((Map<String, Object>)source.get("performance_metrics"));
        modelDTO.setBuilderConfig((Map<String, Object>)source.get("builder_config"));
        modelDTO.setTrainingDataset((Map<String, Object>)source.get("training_dataset"));
        modelDTO.setFeatureSignificance((List<List<Object>>)source.get("feature_significance"));
        modelDTO.setLibrary((String)source.get("library"));
        modelDTO.setModelLocation((String)source.get("model_location"));
        modelDTO.setStatus((String)source.get("status"));
        modelDTO.setOwner((String)source.get("owner"));
        modelDTO.setCreatedDate(new DateTime((String)source.get("created_date")).toDate());
        modelDTO.setDeployedDate(new DateTime((String)source.get("deployed_date")).toDate());
        modelDTO.setReplicationFactor((String)source.get("replication_factor"));
        modelDTO.setPredictionUrl((String)source.get("prediction_url"));
        modelDTO.setNotificationValue((Boolean)source.get("notification_value"));
        return modelDTO;
    }

    public List<GetAllModelsDTO> getAllModels() throws IOException { 
        
        Map<String, String> params = Collections.emptyMap();
        String jsonString = "{\"query\":{\"match\": {\"owner\": \""+SecurityUtils.getCurrentUserLogin().get()+"\"}}}";
        HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
        Response response = restClient.performRequest("GET", "/models/_doc/_search", params, entity);
        String responseBody = EntityUtils.toString(response.getEntity());
        log.debug("response : {}", responseBody);
        
        ListofHitsDTO listOfHitsDTO = mapper.readValue(responseBody, ListofHitsDTO.class);

        List<GetAllModelsDTO> arrayList = new ArrayList<>();
        
        for(int i=0;i<listOfHitsDTO.getHits().getHits().size();i++) {
        GetAllModelsDTO getAllModels = new GetAllModelsDTO();
        getAllModels.setId(listOfHitsDTO.getHits().getHits().get(i).getSource().getId());
        getAllModels.setName(listOfHitsDTO.getHits().getHits().get(i).getSource().getName());
        getAllModels.setType(listOfHitsDTO.getHits().getHits().get(i).getSource().getType());
        getAllModels.setAlgorithm(listOfHitsDTO.getHits().getHits().get(i).getSource().getAlgorithm());
        getAllModels.setProject(listOfHitsDTO.getHits().getHits().get(i).getSource().getProject());
        getAllModels.setVersion(listOfHitsDTO.getHits().getHits().get(i).getSource().getVersion());
        getAllModels.setStatus(listOfHitsDTO.getHits().getHits().get(i).getSource().getStatus());
        getAllModels.setOwner(listOfHitsDTO.getHits().getHits().get(i).getSource().getOwner().toString());
        getAllModels.setAccuracy(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("auc"));
        getAllModels.setAccuracy(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("accuracy"));
        getAllModels.setAccuracy(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("precision"));
        getAllModels.setAccuracy(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("recall"));
        getAllModels.setLibrary(listOfHitsDTO.getHits().getHits().get(i).getSource().getLibrary());
        getAllModels.setCountOfFeatures(listOfHitsDTO.getHits().getHits().get(i).getSource().getFeatureSignificance().size());
        getAllModels.setCreatedDate(listOfHitsDTO.getHits().getHits().get(i).getSource().getCreatedDate());
        arrayList.add(getAllModels);
        }
        return arrayList;
    }

    public Result delete(String id) throws IOException {
        DeleteResponse deleteResponse = null ; 
        
        log.debug("Request to Delete model with ID {}", id);
        try {
            DeleteRequest request = new DeleteRequest(index, docType, id); 
            deleteResponse = client.delete(request);
        } catch (ElasticsearchException exception) {
            if (exception.status() == RestStatus.CONFLICT) {
                log.debug("Error while deleteing model {}",exception);
                return null;
            }
        }
        log.debug("Delete Response {}", deleteResponse.getResult());
        return deleteResponse.getResult();        
    }

    public CreateModelDTO updateModelLocation(String id, String location) throws IOException {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("model_location", location);
        UpdateRequest request = new UpdateRequest(index,docType,id)
        .doc(jsonMap);
        client.update(request);
        return getModelById(id);
    }

    public CreateModelDTO updateModelAfterDeployment(String id, String replicas, String predictionUrl) throws IOException {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("deployed_date", Date.from(Instant.now()));
        jsonMap.put("status", "Deployed");
        jsonMap.put("replication_factor", replicas);
        jsonMap.put("prediction_url", predictionUrl);
        UpdateRequest request = new UpdateRequest(index,docType,id)
        .doc(jsonMap);
        client.update(request);
        return getModelById(id);
    }

    public CreateModelDTO updateModelAfterUnDeployment(String id) throws IOException {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("deployed_date", null);
        jsonMap.put("status", "Experiment");
        jsonMap.put("replication_factor", null);
        jsonMap.put("prediction_url", "");
        UpdateRequest request = new UpdateRequest(index,docType,id)
        .doc(jsonMap);
        client.update(request);
        return getModelById(id);
    }

    public CreateModelDTO updateAfterScaling(String id, String replicas) throws IOException {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("replication_factor", replicas);
        UpdateRequest request = new UpdateRequest(index,docType,id)
        .doc(jsonMap);
        client.update(request);
        return getModelById(id);
    }
    
    public CreateModelDTO updateNotificationValue(String id, Boolean notificationValue) throws IOException {
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("notification_value", notificationValue);
        UpdateRequest request = new UpdateRequest(index,docType,id)
        .doc(jsonMap);
        client.update(request);
        return getModelById(id);
    }

    public long getCount() throws IOException {
        return getAllModels().size();
    }

    public List<GetAllModelsDTO> getAllModelsByProject(String project) throws IOException {
        log.debug("get All Models By Project Method called");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder(); 
        sourceBuilder.from(0);
        sourceBuilder.query(QueryBuilders.matchQuery("project", project));
        sourceBuilder.size(50);
        sourceBuilder.timeout(new TimeValue(120, TimeUnit.SECONDS));
        
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.source(sourceBuilder);
        searchRequest.types(docType);
        
        SearchResponse searchResponse = client.search(searchRequest);
        
        log.debug("search response {} ", searchResponse);
        
        Map<String, Object> list = mapper.readValue(mapper.writeValueAsString(searchResponse), Map.class);
        ListofHitsDTO listOfHitsDTO = mapper.convertValue(list, ListofHitsDTO.class);

        List<GetAllModelsDTO> arrayList = new ArrayList<>();
        
        for(int i=0;i<searchResponse.getHits().getTotalHits();i++) {
        GetAllModelsDTO getAllModels = new GetAllModelsDTO();
        getAllModels.setId(listOfHitsDTO.getHits().getHits().get(i).getId());
        getAllModels.setName(listOfHitsDTO.getHits().getHits().get(i).getSource().getName());
        getAllModels.setType(listOfHitsDTO.getHits().getHits().get(i).getSource().getType());
        getAllModels.setAlgorithm(listOfHitsDTO.getHits().getHits().get(i).getSource().getAlgorithm());
        getAllModels.setProject(listOfHitsDTO.getHits().getHits().get(i).getSource().getProject());
        getAllModels.setVersion(listOfHitsDTO.getHits().getHits().get(i).getSource().getVersion());
        getAllModels.setStatus(listOfHitsDTO.getHits().getHits().get(i).getSource().getStatus());
        getAllModels.setOwner(listOfHitsDTO.getHits().getHits().get(i).getSource().getOwner().toString());
        getAllModels.setCountOfFeatures(listOfHitsDTO.getHits().getHits().get(i).getSource().getFeatureSignificance().size());
        getAllModels.setAuc(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("auc"));
        getAllModels.setAccuracy(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("accuracy"));
        getAllModels.setPrecision(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("precision"));
        getAllModels.setRecall(listOfHitsDTO.getHits().getHits().get(i).getSource().getPerformanceMetrics().get("recall"));
        getAllModels.setCreatedDate(listOfHitsDTO.getHits().getHits().get(i).getSource().getCreatedDate());
        getAllModels.setLibrary((String)listOfHitsDTO.getHits().getHits().get(i).getSource().getLibrary());
        if(getAllModels.getOwner().equals(SecurityUtils.getCurrentUserLogin().get())) {
        arrayList.add(getAllModels);
        }
        }
        return arrayList;
    }

    public List<GetAllProjectsDTO> getAllProject() throws IOException {
        log.debug("get All Projects {}");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.from(0);
        sourceBuilder.size(5000);
        sourceBuilder.timeout(new TimeValue(120, TimeUnit.SECONDS));
        
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.source(sourceBuilder);
        searchRequest.types(docType);
        
        SearchResponse searchResponse = client.search(searchRequest);
        
        log.debug("search response {} ", searchResponse);
        
        Map<String, Object> list = mapper.readValue(mapper.writeValueAsString(searchResponse), Map.class);
        
        ListofHitsDTO listOfHitsDTO = mapper.convertValue(list, ListofHitsDTO.class);
        
        List<String> projects = new ArrayList<String>();

        List<GetAllProjectsDTO> arrayList = new ArrayList<>();
        
        for(int i=0;i<searchResponse.getHits().getTotalHits();i++) {
        String project = listOfHitsDTO.getHits().getHits().get(i).getSource().getProject();
        if(listOfHitsDTO.getHits().getHits().get(i).getSource().getOwner().equals(SecurityUtils.getCurrentUserLogin().get())) {
        projects.add(project);
        }
        }
        
        List<String> listDistinctProjects = projects.stream().distinct().collect(Collectors.toList());
        log.debug("list of distinct projects : {}",listDistinctProjects);

        for(int i=0;i<listDistinctProjects.size();i++) {
            GetAllProjectsDTO getAllProjects = new GetAllProjectsDTO();
            getAllProjects.setProject(listDistinctProjects.get(i));
            getAllProjects.setNumberOfModels(getAllModelsByProject(listDistinctProjects.get(i)).size());
            arrayList.add(getAllProjects);
            }
            return arrayList;
    }

    public List<GetAllProjectsNamesDTO> getAllProjectsNames() throws ParseException, IOException {
        log.debug("into get all projects names");

        Map<String, String> params = Collections.emptyMap();
        String jsonString = "{\"size\":\"0\",\"aggs\":{\"unique_projects\":{\"terms\":{\"field\":\"project\"}}}}";
        HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
        Response response = restClient.performRequest("GET", "/models/_doc/_search", params, entity);
        String responseBody = EntityUtils.toString(response.getEntity());
        log.debug("response : {}", responseBody);

        ListOfProjectsNamesDTO listOfProjectNames = mapper.readValue(responseBody, ListOfProjectsNamesDTO.class);

        List<GetAllProjectsNamesDTO> arrayList = new ArrayList<>();

        for (int i = 0; i < listOfProjectNames.getAggregations().getUnique_projects().getBuckets().size(); i++) {
            GetAllProjectsNamesDTO getAllProjectsNamesDTO = new GetAllProjectsNamesDTO();
            getAllProjectsNamesDTO.setProjectName(
                    listOfProjectNames.getAggregations().getUnique_projects().getBuckets().get(i).getKey());
            arrayList.add(getAllProjectsNamesDTO);
        }
        return arrayList;
    }
}
