package com.predera.dmml.service;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.predera.dmml.ModelmanagerApp;
import com.predera.dmml.config.ApplicationProperties;
import com.predera.dmml.service.dto.PredictionDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ModelmanagerApp.class)
public class DataServiceTest {

    @Autowired
    private RestHighLevelClient client;

    private PredictionsDataService dataService;
    
    private ApplicationProperties applicationProperties;

    @Before
    public void setup() {
        this.dataService = new PredictionsDataService(client, applicationProperties, null);
    }

    @Test
    public void testSaveFeedback() throws IOException {
        PredictionDTO prediction = new PredictionDTO();
        prediction.setIndex("predictions");
        prediction.setType("prediction");
        prediction.setModelId("951");
        prediction.setModelName("test-model");
        prediction.setProject("test-project");
        prediction.setVersion("1");
        prediction.setUid("1024");
        Map<String, Object> predictionInput = new HashMap<>();
        predictionInput.put("age", 25d);
        predictionInput.put("gender", 1d);
        prediction.setPredictionInput(predictionInput);
        prediction.setPredictionDate(null);
        prediction.setPredictionOutput(1f);
        Map<String, Object> predictionReasons = new HashMap<>();
        predictionReasons.put("age", 25f);
        predictionReasons.put("gender", 1f);
        prediction.setPredictionReasons(predictionReasons);

        PredictionDTO savedPrediction = dataService.savePrediction(prediction);

        Assert.assertNotNull(savedPrediction);
        Assert.assertNotNull(savedPrediction.getId());
    }

}
